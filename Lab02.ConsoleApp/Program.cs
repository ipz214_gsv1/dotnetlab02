﻿using Lab02.ConsoleApp;

//string inputstring = "hello world";

//Console.WriteLine(inputstring.Inversion());
//Console.WriteLine(inputstring.CountOfSameItems('o'));


//int[] array = new int[10] { 1, 2, 3, 1, 2, 3, 1, 2, 3, 2 };

//Console.WriteLine($"2 зустрічається в масиві {array.CountOfTimes(2)} разів");

//var newArray = array.UniqueItems();


//Console.WriteLine("Масив унікальних елементів: ");
//foreach (var item in newArray)
//{
//    Console.WriteLine(item);
//}

try
{

    ExtendedDictionary<int, int, int> dict = new ExtendedDictionary<int, int, int>();

    dict.Add(
        new ExtendedDictionaryElement<int, int, int>() 
            { Key = 1, FirstValue = 1, SecondValue = 1 });

    Console.WriteLine($"Кількість елементів у словнику: {dict.Count}"); ;

    Console.WriteLine($"Чи існує елемент за ключем: {dict.CheckIfExistsByKey(1)}");

    Console.WriteLine($"Чи існує елемент за значеннями: {dict.CheckIfExistsByValues(2, 1)}");

    Console.WriteLine($"Перше значення за ключем {dict[1].FirstValue}");
    Console.WriteLine($"Перше значення за ключем {dict[1].SecondValue}");

    dict[1].FirstValue = 101;

    Console.WriteLine(dict[1].FirstValue);

    dict.DeleteByKey(1);

    Console.WriteLine($"Кількість елементів у словнику: {dict.Count}");

    dict.Add(
       new ExtendedDictionaryElement<int, int, int>() { Key = 1, FirstValue = 1, SecondValue = 1 });

    dict.Add(
       new ExtendedDictionaryElement<int, int, int>() { Key = 2, FirstValue = 1, SecondValue = 1 });

    dict.Add(
       new ExtendedDictionaryElement<int, int, int>() { Key = 3, FirstValue = 1, SecondValue = 1 });


    Console.WriteLine($"Elems with foreach loop: ");

    foreach (var item in dict)
    {
        Console.WriteLine(item.Key);
    }
}

catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
