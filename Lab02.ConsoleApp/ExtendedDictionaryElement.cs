﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab02.ConsoleApp
{
    public class ExtendedDictionaryElement<T, U, V> 
    { 
        public T Key { get; set; }
        public U FirstValue { get; set; }
        public V SecondValue { get; set; }
    }
}
