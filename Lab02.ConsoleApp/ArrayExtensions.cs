﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab02.ConsoleApp
{
    public static class ArrayExtensions
    {
        public static int CountOfTimes<T>(this T[] array, T item)
        {
            if (array.Length <= 0)
                return 0;

            return array.Where(x => x.Equals(item) == true).Count();
        }

        public static T[] UniqueItems<T>(this T[] array)
        {
            return array.Distinct().ToArray();
        }
    }
}
