﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab02.ConsoleApp
{
    public static class StringExtensions
    {
        public static string Inversion(this string str)
        {
            StringBuilder sb = new StringBuilder(str);

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == char.ToUpper(str[i]))
                {
                    sb[i] = char.ToLower(sb[i]);
                }
                else
                {
                    sb[i] = char.ToUpper(sb[i]);
                }
            }

            return sb.ToString();
        }

        public static int CountOfSameItems(this string str, char item)
        {
            return str.Where(x => x == item).Count();
        }
    }
}
