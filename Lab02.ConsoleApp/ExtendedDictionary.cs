﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab02.ConsoleApp
{
    public class ExtendedDictionary<T, U, V> : IEnumerable<ExtendedDictionaryElement<T, U, V>>
    {
        public List<ExtendedDictionaryElement<T, U, V>> CurrentValues { get; set; } = 
            new List<ExtendedDictionaryElement<T, U, V>>();
        public int Count 
        {   
            get
            {
                return CurrentValues.Count();
            } 
        }

        public ExtendedDictionaryElement<T, U, V> this[T keyValue]
        {
            get
            {
                var item = CurrentValues.
                    FirstOrDefault(x => x.Key.Equals(keyValue));

                if (item == null)
                    throw new KeyNotFoundException("No item found with such key");

                return item;
            }
        }

        public void Add(ExtendedDictionaryElement<T, U, V> item)
        {
            CurrentValues.Add(item);
        }

        public void DeleteByKey(T keyValue)
        {
            var item = CurrentValues.
                FirstOrDefault(x => x.Key.Equals(keyValue));

            if (item == null)
                throw new KeyNotFoundException("No item found with such key");

            CurrentValues.Remove(item);
        }

        public bool CheckIfExistsByKey(T keyValue)
        {
            var result = 
                CurrentValues.FirstOrDefault(x => x.Key.Equals(keyValue));

            if (result == null)
                return false;

            return true;
        }

        public bool CheckIfExistsByValues(U firstValue, V secondValue)
        {
            var result =
                CurrentValues.
                    FirstOrDefault(x => x.FirstValue.Equals(firstValue) &&
                                   x.SecondValue.Equals(secondValue));

            if (result == null)
                return false;

            return true;
        }

        public IEnumerator<ExtendedDictionaryElement<T, U, V>> GetEnumerator()
        {
            return CurrentValues.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return CurrentValues.GetEnumerator();
        }
    }
}
